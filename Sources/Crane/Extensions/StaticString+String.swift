internal extension StaticString {
  var string: String { self.description }
}
