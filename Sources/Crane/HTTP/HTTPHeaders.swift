import Foundation

/// Structured collection of http header fields
public struct HTTPHeaders {

  private var store: [HTTPHeader] = .init()
  /// Raw representation of headers
  public var rawDictionary: [String: String] { Dictionary(uniqueKeysWithValues: store.map { ($0.name, $0.value) }) }

  public init(from anyDictionary: [AnyHashable: Any]) {
    anyDictionary
    .compactMap { (key: Any, value: Any) -> (String, String)? in
      guard
        let stringKey = key as? String,
        let stringValue = value as? String
      else { return nil }
      return (stringKey, stringValue)
    }
    .map(HTTPHeader.init).forEach { header in
      if let updateIndex = store.firstIndex(where: { $0.name == header.name }), header.name != "Set-Cookie" {
        store[updateIndex] = header
      } else {
        store.append(header)
      }
    }
  }

  public mutating func set(_ header: HTTPHeader) {
    if let updateIndex = store.firstIndex(where: { $0.name == header.name }), header.name != "Set-Cookie" {
      store[updateIndex] = header
    } else {
      store.append(header)
    }
  }

  public subscript(name: String) -> String? {
    get {
      store.first(where: { $0.name == name })?.value
    }
    set {
      if let value = newValue {
        set(HTTPHeader(name, value: value))
      } else {
        store.removeAll(where: { $0.name == name })
      }
    }
  }

  public func updated(with other: HTTPHeaders) -> HTTPHeaders {
    var result: HTTPHeaders = self
    other.store.forEach { header in
      if let updateIndex = store.firstIndex(where: { $0.name == header.name }), header.name != "Set-Cookie" {
        result.store[updateIndex] = header
      } else {
        result.store.append(header)
      }
    }
    return result
  }
}

extension HTTPHeaders: ExpressibleByDictionaryLiteral {
  public init(dictionaryLiteral: (String, String)...) {
    dictionaryLiteral.map(HTTPHeader.init).forEach { header in
      if let updateIndex = store.firstIndex(where: { $0.name == header.name }), header.name != "Set-Cookie" {
        store[updateIndex] = header
      } else {
        store.append(header)
      }
    }
  }
}

extension HTTPHeaders: ExpressibleByArrayLiteral {
  public init(arrayLiteral: HTTPHeader...) {
    arrayLiteral.forEach { header in
      if let updateIndex = store.firstIndex(where: { $0.name == header.name }), header.name != "Set-Cookie" {
        store[updateIndex] = header
      } else {
        store.append(header)
      }
    }
  }
}
