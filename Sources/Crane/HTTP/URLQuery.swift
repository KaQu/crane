import Foundation

/// Structured collection of url query items
public struct URLQuery {

  private var store: [URLQueryItem] = .init()
  public var items: [URLQueryItem] { store }
  public var isEmpty: Bool { store.isEmpty }

  public mutating func set(_ queryItem: URLQueryItem) {
    if let updateIndex = store.firstIndex(where: { $0.name == queryItem.name }) {
      store[updateIndex] = queryItem
    } else {
      store.append(queryItem)
    }
  }

  public subscript(itemName: String) -> String? {
    get {
      store.first(where: { $0.name == itemName })?.value
    }
    set {
      if let value = newValue {
        set(URLQueryItem(name: itemName, value: value))
      } else {
        store.removeAll(where: { $0.name == itemName })
      }
    }
  }

  public func updated(with other: URLQuery) -> URLQuery {
    var result: URLQuery = self
    other.store.forEach { queryItem in
      if let updateIndex = store.firstIndex(where: { $0.name == queryItem.name }) {
        result.store[updateIndex] = queryItem
      } else {
        result.store.append(queryItem)
      }
    }
    return result
  }
}

extension URLQuery: ExpressibleByDictionaryLiteral {
  public init(dictionaryLiteral: (String, String)...) {
    dictionaryLiteral.map(URLQueryItem.init).forEach { queryItem in
      if let updateIndex = store.firstIndex(where: { $0.name == queryItem.name }) {
        store[updateIndex] = queryItem
      } else {
        store.append(queryItem)
      }
    }
  }
}

extension URLQuery: ExpressibleByArrayLiteral {
  public init(arrayLiteral: URLQueryItem...) {
    arrayLiteral.forEach { queryItem in
      if let updateIndex = store.firstIndex(where: { $0.name == queryItem.name }) {
        store[updateIndex] = queryItem
      } else {
        store.append(queryItem)
      }
    }
  }
}

/*
extension URLQuery: RawRepresentable {
  public init?(rawValue: String) {}

  public var rawValue: String {}
}
 */
