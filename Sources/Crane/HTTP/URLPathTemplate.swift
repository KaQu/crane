import Foundation

/// Template for URL path used to make acctual urls.
/// Might contain parameters that will be resolved dynamically.
/// Parameters should be specified by names between `{` and `}`
/// in path i.e. `/service/{id}` contains parameter "id".
public struct URLPathTemplate {
  private var parts: Array<Part>

  private enum Part {
    case raw(String)
    case parameter(String)
  }

  public func buildURL(
    from components: URLComponents,
    with parameters: URLParameters,
    and query: URLQuery
  ) -> Result<URL, NetworkError>
  {
    var components = components
    do {
      components.path
      = try "/"
      + parts
      .map {
        switch $0 {
        case let .raw(part): return part
        case let .parameter(name):
          guard let parameterValue = parameters[name]
          else { throw NetworkError.missingURLParameter }
          return parameterValue
        }
      }
      .joined(separator: "/")
    } catch NetworkError.missingURLParameter {
      return .failure(NetworkError.missingURLParameter)
    } catch { fatalError("Unreachable") }
    components.queryItems = query.items
    guard let url = components.url
      else { return .failure(NetworkError.invalidURL) }
    return .success(url)
  }
}

extension URLPathTemplate: ExpressibleByStringLiteral {
  public init(stringLiteral: StaticString) {
    self.parts
      = stringLiteral
      .string
      .split(separator: "/")
      .map {
        if $0.hasPrefix("{") && $0.hasSuffix("}") {
          let parameterName: Substring = $0.dropFirst().dropLast()
          precondition(!parameterName.isEmpty, "Invalid URLPathTemplate")
          return .parameter(String(parameterName))
        } else if $0.allSatisfy(isURLAllowed) {
          return .raw(String($0))
        } else {
          fatalError("Invalid URLPathTemplate")
        }
      }
  }
}
