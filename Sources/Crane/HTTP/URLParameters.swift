/// Structured collection of url parameters
public struct URLParameters {
  private var store: [URLParameter] = .init()

  public mutating func set(_ parameter: URLParameter) {
    if let updateIndex = store.firstIndex(where: { $0.name == parameter.name }) {
      store[updateIndex] = parameter
    } else {
      store.append(parameter)
    }
  }

  public subscript(name: String) -> String? {
    get {
      store.first(where: { $0.name == name })?.value
    }
    set {
      if let value = newValue {
        set(URLParameter(name, value: value))
      } else {
        store.removeAll(where: { $0.name == name })
      }
    }
  }

  public func updated(with other: URLParameters) -> URLParameters {
    var result: URLParameters = self
    other.store.forEach { parameter in
      if let updateIndex = store.firstIndex(where: { $0.name == parameter.name }) {
        result.store[updateIndex] = parameter
      } else {
        result.store.append(parameter)
      }
    }
    return result
  }
}

extension URLParameters: ExpressibleByDictionaryLiteral {
  public init(dictionaryLiteral: (String, String)...) {
    dictionaryLiteral.map(URLParameter.init).forEach { parameter in
      if let updateIndex = store.firstIndex(where: { $0.name == parameter.name }) {
        store[updateIndex] = parameter
      } else {
        store.append(parameter)
      }
    }
  }
}

extension URLParameters: ExpressibleByArrayLiteral {
  public init(arrayLiteral: URLParameter...) {
    arrayLiteral.forEach { parameter in
      if let updateIndex = store.firstIndex(where: { $0.name == parameter.name }) {
        store[updateIndex] = parameter
      } else {
        store.append(parameter)
      }
    }
  }
}
