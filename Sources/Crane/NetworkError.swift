import Foundation

// TODO: revisit error handling
public enum NetworkError: Error {
  case unsupportedHTTPVersion
  case invalidURL
  case missingURLParameter
  case invalidURLQuery
  case invalidHTTPHeaders
  case unableToEncodeRequestBody(Error)
  case unableToMakeRequest(Error)
  case invalidResponseStatusCode
  case unableToDecodeResponse(HTTPResponse)
  case internalInconsistency
  case sessionClosed
}
