import Foundation

public protocol NetworkResponse {
  static func from(_ response: HTTPResponse) -> Result<Self, NetworkError>
}
