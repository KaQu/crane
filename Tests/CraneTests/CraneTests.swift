import XCTest
@testable import Crane

protocol FoundationNetworkRequest: NetworkRequest where Session == FoundationSession {}
protocol GetNetworkRequest: NetworkRequest where Body == Void {}
extension GetNetworkRequest { static var httpMethod: HTTPMethod { .get } }

struct SampleRequest: FoundationNetworkRequest, GetNetworkRequest {
  
  static var urlPathTemplate: URLPathTemplate = "/base64/{base64}"
  
  static var urlParameters: URLParameters { [URLParameter("base64", value: "SFRUUEJJTiBpcyBhd2Vzb21l")] }
  
  struct Response: NetworkResponse {
    static func from(_ response: HTTPResponse) -> Result<Self, NetworkError> {
      guard response.statusCode == .ok
        else { return .failure(.invalidResponseStatusCode)}
      guard let string = String(data: response.body, encoding: .utf8)
        else { return .failure(.unableToDecodeResponse(response)) }
      return .success(.init(stringBody: string))
    }
    var stringBody: String
  }
  
}

final class CraneTests: XCTestCase {
  func testExample() {
    let responseExpectation = expectation(description: "Responds eventually")
    
    let ses = FoundationSession(host: "httpbin.org")
    ses.make(SampleRequest()) { (res) in
      responseExpectation.fulfill()
    }
    
    wait(for: [responseExpectation], timeout: 5)
  }
  
  static var allTests = [
    ("testExample", testExample),
  ]
}
