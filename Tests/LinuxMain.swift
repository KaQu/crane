import XCTest

import CraneTests

var tests = [XCTestCaseEntry]()
tests += CraneTests.allTests()
XCTMain(tests)
